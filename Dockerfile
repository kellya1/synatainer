# syntax=docker/dockerfile:1

FROM golang:1.16-alpine as builder

WORKDIR /app

COPY stui/go.mod ./
COPY stui/go.sum ./
RUN go mod download

COPY stui/*.go ./
ARG STUI_VERSION=dockerbuild
RUN go build -ldflags="-X main.version=$STUI_VERSION" -o /stui

FROM registry.gitlab.com/mb-saces/rust-synapse-compress-state:latest

RUN apk add --no-cache bash busybox-suid curl jq postgresql-client
RUN apk add tzdata

COPY scripts/ /usr/local/bin

#sendmail from hack
COPY sendmail.wrapper /usr/local/sbin/sendmail

COPY entrypoint.sh /entrypoint.sh
COPY setup-crontab.sh /setup-crontab.sh

COPY --from=builder /stui /usr/local/bin/stui

ENTRYPOINT [ "/entrypoint.sh" ]
