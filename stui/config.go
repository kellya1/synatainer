package main

import (
	"fmt"
	"os"
	"path"

	"github.com/joho/godotenv"
)

// Make a variable for the config filename envirionment variable name, can be set at build time.
var cfgFileEnvName = "SYNATAINER_CONFIGFILE"

// did we load the config files?
var isLoaded bool = false

// settings from config file
var confFileEnv map[string]string

func simpleLoadConfig(filename string) map[string]string {
	envMap, err := godotenv.Read(filename)
	if err != nil {
		panic(err)
	}
	return envMap
}

func loadConfigFile() {
	if isLoaded {
		return
	}

	if len(configFile) == 0 {
		val, ok := os.LookupEnv("SYNATAINER_CONFIGFILE")
		if ok {
			configFile = val
		}
	}

	if len(configFile) > 0 {
		// explicit config file, load and ignore defaults
		confFileEnv = simpleLoadConfig(configFile)
		isLoaded = true
		return
	}

	if checkContainer() {
		cfgFile := "/conf/synatainer.conf"
		if fileExists((cfgFile)) {
			confFileEnv = simpleLoadConfig(cfgFile)
		} else {
			confFileEnv = make(map[string]string)
		}
		isLoaded = true
		return
	}

	// still here? load desktop conf...
	globalCfgName := "/etc/synatainer/synatainer.conf"
	userCfgName := path.Join(getUserConfigDir(), "synatainer/synatainer.conf")

	globalExists := fileExists(globalCfgName)
	userExists := fileExists(userCfgName)

	if globalExists && userExists {
		// booth files exist, load an merge them
		confFileEnv = simpleLoadConfig(globalCfgName)
		userEnv := simpleLoadConfig(userCfgName)
		for k, v := range userEnv {
			confFileEnv[k] = v
		}
	} else if globalExists {
		confFileEnv = simpleLoadConfig(globalCfgName)
	} else if userExists {
		confFileEnv = simpleLoadConfig(userCfgName)
	} else {
		confFileEnv = make(map[string]string)
	}

	isLoaded = true
}

func setStringPointerVar(variable interface{}, value string) {
	v, _ := (variable).(*string)
	*v = value
}

func resolveConfigItemString(cmdLineItem *string, envItem string, defaultValue string) {
	if len(*cmdLineItem) > 0 {
		// already set from comaandline, nothing to do.
		return
	}
	// not set on cmd line, look up env
	val, ok := os.LookupEnv(envItem)
	if ok {
		setStringPointerVar(cmdLineItem, val)
		return
	}
	// still not set, look up config file
	// ensure config file is loaded
	loadConfigFile()
	val, ok = confFileEnv[envItem]
	if ok {
		setStringPointerVar(cmdLineItem, val)
		return
	}
	// still here? return default
	//cmdLineItem = &defaultValue
	setStringPointerVar(cmdLineItem, defaultValue)
}

func resolveDBConfig() {
	resolveConfigItemString(&db_user, "DB_USER", "synapse_user")
	resolveConfigItemString(&db_host, "DB_HOST", "postgres:5432")
	resolveConfigItemString(&db_name, "DB_NAME", "synapse")
	resolveConfigItemString(&db_pass, "PGPASSWORD", "")
}

func print_pgpass_line() {
	resolveDBConfig()
	fmt.Printf("%s:%s:%s:%s\n", db_host, db_name, db_user, db_pass)
}

func getPostgresConnectString() (pgstring string) {
	resolveDBConfig()
	// urlExample := "postgres://username:password@localhost:5432/database_name"
	pgstring = fmt.Sprintf("postgres://%s:%s@%s/%s",
		db_user,
		db_pass,
		db_host,
		db_name)
	return
}
