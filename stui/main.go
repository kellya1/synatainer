package main

import (
	"fmt"

	"github.com/integrii/flaggy"
)

// Make a variable for the version which will be set at build time.
var version = "unknown"

// Keep subcommands as globals so you can easily check if they were used later on.
var (
	cmdCron         *flaggy.Subcommand
	cmdCronhandler  *flaggy.Subcommand
	cmdDBShell      *flaggy.Subcommand
	cmdDBInfo       *flaggy.Subcommand
	cmdPGPassExport *flaggy.Subcommand
	cmdInfo         *flaggy.Subcommand
	cmdVacuum       *flaggy.Subcommand
)

// Setup the variables you want your incoming flags to set.
var configFile string

var (
	db_user string
	db_host string
	db_name string
	db_pass string
)

// include tokens and passords in config info
var infoPrivate bool

func addDBParams(cmd *flaggy.Subcommand) {
	cmd.String(&db_user, "u", "user", "database user")
	cmd.String(&db_host, "s", "host", "datbase host")
	cmd.String(&db_name, "n", "name", "database name")
	cmd.String(&db_pass, "p", "password", "database pasword")
}

func init() {
	// Set your program's name and description.  These appear in help output.
	flaggy.SetName("stui")
	flaggy.SetDescription("Synatainer Terminal User Interface")

	// You can disable various things by changing bools on the default parser
	// (or your own parser if you have created one).
	flaggy.DefaultParser.ShowHelpOnUnexpected = true

	// You can set a help prepend or append on the default parser.
	flaggy.DefaultParser.AdditionalHelpPrepend = "You may find more information at https://gitlab.com/mb-saces/synatainer"

	// You can set a help prepend or append on the default parser.
	flaggy.DefaultParser.AdditionalHelpAppend = "\nStart stui without parameters or commands to obtain an interactive user interface"

	// global flags
	flaggy.String(&configFile, "c", "config", "Config file to use")

	cmdCron = flaggy.NewSubcommand("cron")
	cmdCron.Description = "My great stub subcommand!"

	cmdCronhandler = flaggy.NewSubcommand("cronhandler")
	cmdCronhandler.Description = "My great stub subcommand!"

	cmdInfo = flaggy.NewSubcommand("info")
	cmdInfo.Description = "Show configuration"
	cmdInfo.Bool(&infoPrivate, "p", "private", "Show private tokens and passwords")

	cmdDBShell = flaggy.NewSubcommand("db-shell")
	cmdDBShell.Description = "Open a database shell, psql like"
	addDBParams(cmdDBShell)

	cmdDBInfo = flaggy.NewSubcommand("db-info")
	cmdDBInfo.Description = "Prints some database stats"
	addDBParams(cmdDBInfo)

	cmdVacuum = flaggy.NewSubcommand("vacuum-db")
	cmdVacuum.Description = "Vacuum the database"
	addDBParams(cmdVacuum)

	cmdPGPassExport = flaggy.NewSubcommand("pgpassexport")
	cmdPGPassExport.Description = "Prints the line for the PGPASSFILE"
	addDBParams(cmdPGPassExport)

	// Add the subcommand to the parser at position 1
	flaggy.AttachSubcommand(cmdCron, 1)
	flaggy.AttachSubcommand(cmdCronhandler, 1)
	flaggy.AttachSubcommand(cmdInfo, 1)
	flaggy.AttachSubcommand(cmdVacuum, 1)
	flaggy.AttachSubcommand(cmdDBShell, 1)
	flaggy.AttachSubcommand(cmdDBInfo, 1)
	flaggy.AttachSubcommand(cmdPGPassExport, 1)

	// Set the version and parse all inputs into variables.
	flaggy.SetVersion(version)
	flaggy.Parse()
}

func main() {
	//	loadConfig()
	if cmdCron.Used {
		fmt.Println("Cron.")
	} else if cmdCronhandler.Used {
		fmt.Println("CronHandler.")
	} else if cmdVacuum.Used {
		fmt.Println("Vacuum. Weltraumputze oder so. Spreche kein postgres.")
	} else if cmdDBShell.Used {
		fmt.Println("psql emulator")
	} else if cmdDBInfo.Used {
		db_info()
	} else if cmdInfo.Used {
		show_config_info(infoPrivate)
	} else if cmdPGPassExport.Used {
		print_pgpass_line()
	} else {
		// empty command line, start tui
		stui_tui()
	}
}
