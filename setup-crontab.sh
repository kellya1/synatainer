#!/bin/sh

# if i figure out the syntax this may go inlined in entrypoint

set -eu

CONFIG_FILE=/conf/synatainer.conf && test -f $CONFIG_FILE && source $CONFIG_FILE

if [ "${SYNATAINER_CRONSPEC+1}" ]; then
    CRONSPEC="${SYNATAINER_CRONSPEC}"
else
    CRONSPEC="2 2 * * *"
fi

(echo "${CRONSPEC} /usr/local/bin/synatainer-cron.sh") | crontab -

if [ -n "${MAILTO:-}" ]; then
  (echo "MAILTO=$MAILTO"; crontab -l) | crontab -
fi

crontab -l
