#!/bin/sh

# tag::doc[]
# Resets the autocompressor
#
# WARNING: This just drops the state compressor tables from the database. Mileage can vary on side effects.
# end::doc[]

set -eux

CONFIG_FILE=/conf/synatainer.conf && test -f $CONFIG_FILE && source $CONFIG_FILE

if [ -n "${PGPASSWORD:-}" ]; then
  export PGPASSWORD
fi

psql_cmd="psql -U $DB_USER -d $DB_NAME -h $DB_HOST"

$psql_cmd -c "DROP TABLE state_compressor_state;"
$psql_cmd -c "DROP TABLE state_compressor_progress;"
$psql_cmd -c "DROP TABLE state_compressor_total_progress;"
