#!/bin/sh

# tag::doc[]
# Synapse maintenance script.
# What it does:
#     purge all rooms without local members
#     run the state autocompressor (small setting)
#     delete old remote media
#     delete old message history
#     run the state autocompressor (big setting)
#     vacuum the database
#
# Usage: synatainer.sh [emergency]
#     (not yet, may become an interactive tool) emergency: do a complete service run with settings to free as much space as possible
# end::doc[]

set -eu

SERVICE_MODE=${1:-none}

echo $SERVICE_MODE

CONFIG_FILE=/conf/synatainer.conf && test -f $CONFIG_FILE && source $CONFIG_FILE

remote_cache_purge.sh

purge_rooms_no_local_members.sh

purge_history.sh

autocompressor-small.sh

vacuum-db.sh
