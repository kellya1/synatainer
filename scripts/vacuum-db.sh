#!/bin/sh

# tag::doc[]
# Vacuum the database
#   Set VACUUM_DB=skip to not vacuum the database
# end::doc[]

set -eu

CONFIG_FILE=/conf/synatainer.conf && test -f $CONFIG_FILE && source $CONFIG_FILE

case "${VACUUM_DB:-yes}" in
  none|off|skip ) echo "Vacuuming database turned off: (${VACUUM_DB}). Bye."
                  exit 0
                  ;;
esac

if [ -n "${PGPASSWORD:-}" ]; then
  export PGPASSWORD
fi

psql_cmd="psql -U $DB_USER -d $DB_NAME -h $DB_HOST"

for table in $($psql_cmd --tuples-only -P pager=off -c "SELECT table_name FROM information_schema.tables WHERE table_schema = 'public';"); do
  $psql_cmd -c "VACUUM FULL VERBOSE $table;"
done
