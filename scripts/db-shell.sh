#!/bin/sh

# tag::doc[]
# Opens a db shell (psql)
# end::doc[]

set -eu

CONFIG_FILE=/conf/synatainer.conf && test -f $CONFIG_FILE && source $CONFIG_FILE

if [ -n "${PGPASSWORD:-}" ]; then
  export PGPASSWORD
fi

psql -U $DB_USER -d $DB_NAME -h $DB_HOST
